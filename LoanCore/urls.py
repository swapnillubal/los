from django.urls import path
from . import views

urlpatterns = [

    path('', views.index, name='index'),
    path('customer_details', views.customer_details, name='customer_details'),
    path('customer_details/new', views.create_customer_details, name='create_customer_details'),
    path('customer_details/<int:id>/', views.update_customer_details, name='update_customer_details'),
    # Issue form views
    path('form_issue_details/', views.form_issue_details, name='form_issue_details'),
    path('form_issue_details/new/<int:id>/', views.create_form_issue, name='create_form_issue'),
    # Loan Inward Form views
    path('loan_inward_details/', views.loan_inwardform_details, name='loan_inwardform_details'),
    path('loan_inward_details/new/<str:id>/', views.create_form_inward, name='create_form_inward'),
    # Loan Inward Form views
    path('brief_profile_details/', views.brief_profile_details, name='brief_profile_details'),
    path('brief_profile_details/new/<str:id>/', views.create_form_brief_profile, name='create_form_brief_profile'),

    #Working capitals
    path('working_capital_limit_details/', views.working_capital_limit_details, name='working_capital_limit_details'),
    path('working_capital_limit_details/new/<str:id>', views.create_working_capital_limit, name='create_working_capital_limit'),

    #Nature of limits
    path('term_details/', views.term_details, name='term_details'),
    path('term_details/new/<str:id>', views.create_term, name='create_term'),

    #Nature of security
    path('nature_of_security_details/', views.nature_of_security_details, name='nature_of_security_details'),
    path('nature_of_security_details/new/<str:id>', views.create_nature_of_security, name='create_nature_of_security'),

    #Gist of projection
    path('financial_analysis_details/', views.financial_analysis_details, name='financial_analysis_details'),
    path('financial_analysis_details/new/<str:id>', views.create_financial_analysis, name='create_financial_analysis'),

    #Gist of projection
    path('gist_projection_details/', views.gist_projection_details, name='gist_projection_details'),
    path('gist_projection_details/new/<str:id>', views.create_gist_projection, name='create_gist_projection'),

]
