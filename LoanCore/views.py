from datetime import time
import datetime
from django.shortcuts import render, redirect
from django.urls import reverse

from .models import *
from django.contrib.auth.decorators import login_required
from .forms import *
from django.contrib import messages
from django import template
from django.http import HttpResponse, HttpResponseRedirect


# Create your views here.


@login_required
def index(request):
    return render(request, 'LoanCore/index.html', {})


@login_required
def customer_details(request):
    cust_details = cust_detail.objects.all()
    return render(request, 'LoanCore/customer_details.html', {'cust_details': cust_details})


def create_customer_details(request):
    if request.method == 'POST':
        saverecord = cust_detail()
        saverecord.cust_name = request.POST.get('cust_name')
        saverecord.cust_email = request.POST.get('cust_email')
        saverecord.cust_dob = request.POST.get('cust_dob')
        # saverecord.cust_dob = datetime.strptime(request.POST.get['cust_dob'], "%Y-%m-%d")
        saverecord.cust_address = request.POST.get('cust_address')
        saverecord.cust_tel1 = request.POST.get('cust_tel1')
        saverecord.cust_tel2 = request.POST.get('cust_tel2')
        saverecord.save()
        messages.success(request, 'Record Added Successfully')
    else:
        messages.error(request, "Record insert unsuccessful")

    return render(request, 'LoanCore/customer_details_form.html', {})


def update_customer_details(request, id):
    cust_details = cust_detail.objects.get(cust_id=id)

    if request.method == 'POST':
        saverecord = cust_detail()
        saverecord.cust_id = request.POST.get('cust_id')
        saverecord.cust_name = request.POST.get('cust_name')
        saverecord.cust_email = request.POST.get('cust_email')
        saverecord.cust_dob = request.POST.get('cust_dob')
        saverecord.cust_address = request.POST.get('cust_address')
        saverecord.cust_tel1 = request.POST.get('cust_tel1')
        saverecord.cust_tel2 = request.POST.get('cust_tel2')
        saverecord.save()
        messages.success(request, 'Record Added Successfully')
    else:
        messages.error(request, "Record insert unsuccessful")

    return render(request, 'LoanCore/update_customer_details.html', {'cust_details': cust_details})


def form_issue_details(request):
    cust_details = cust_detail.objects.all()
    return render(request, 'LoanCore/form_issue_details.html', {'cust_details': cust_details})


def create_form_issue(request, id):
    cust_details = cust_detail.objects.get(cust_id=id)
    y = datetime.datetime.now()
    datetime_string = "AP" + str(y.year) + str(y.month) + str(y.day) + str(y.hour) + str(y.minute) + str(y.second)
    if request.method == 'POST':
        saverecord = L_App()
        saverecord.cust_id_id = request.POST.get('cust_id')
        saverecord.app_id = datetime_string
        saverecord.issue_date = request.POST.get('issue_date')
        saverecord.loan_amount = request.POST.get('loan_amount')
        saverecord.loan_type = request.POST.get('loan_type')
        saverecord.save()
        messages.success(request, 'Record Added Successfully')
    else:
        messages.error(request, "Record insert unsuccessful")

    return render(request, 'LoanCore/add_form_issue.html', {'cust_details': cust_details})


def loan_inwardform_details(request):
    app_details = L_App.objects.select_related('cust_id')
    return render(request, 'LoanCore/loan_inward_details.html', {'app_details': app_details})


def create_form_inward(request, id):
    l_app_details = L_App.objects.get(l_id=id)
    cust_id = str(l_app_details.cust_id)
    cust_details = cust_detail.objects.get(cust_id=cust_id)
    y = datetime.datetime.now()
    datetime_string = "AP" + str(y.year) + str(y.month) + str(y.day) + str(y.hour) + str(y.minute) + str(y.second)
    if request.method == 'POST':
        saverecord = L_App()
        saverecord.l_id = request.POST.get('l_id')
        saverecord.app_id = str(l_app_details.app_id)
        saverecord.loan_type = str(l_app_details.loan_type)
        saverecord.inward_date = request.POST.get('inward_date')
        saverecord.loan_amount = request.POST.get('loan_amount')
        saverecord.inward_no = request.POST.get('inward_no')
        saverecord.checklist = request.POST.get('checklist')
        saverecord.cust_id_id = cust_id
        saverecord.save()
        messages.success(request, 'Record Added Successfully')
    else:
        messages.error(request, "Record insert unsuccessful")

    return render(request, 'LoanCore/loan_inward_form.html',
                  {'l_app_details': l_app_details, 'cust_details': cust_details})


def brief_profile_details(request):
    app_details = L_App.objects.select_related('cust_id')
    return render(request, 'LoanCore/brief_profile_details.html', {'app_details': app_details})


def create_form_brief_profile(request, id):

    l_app_details = L_App.objects.get(l_id=id)
    cust_id = str(l_app_details.cust_id)
    cust_details = cust_detail.objects.get(cust_id=cust_id)
    if request.method == 'POST':
        # participant data
        par_names = []
        par_annual_incomes = []
        par_net_worths = []
        par_cibil_scores = []

        # guarantors data
        guar_names = []
        guar_annual_incomes = []
        guar_net_worths = []
        guar_cibil_scores = []

        # Nature of loan data
        nature_names = []
        nature_by_partys = []
        nature_by_branchs = []
        nature_by_hos = []

        participants_details = {}
        guarantors_details = {}
        nature_of_loan_details = {}

        # participants name
        par_names = request.POST.getlist('par_name[]')
        par_annual_incomes = request.POST.getlist('par_annual_income[]')
        par_net_worths = request.POST.getlist('par_net_worth[]')
        par_cibil_scores = request.POST.getlist('par_cibil_score[]')

        i = 0
        for x in range(len(par_names)):
            participants_details[i] = {'par_name': par_names[i], 'par_annual_income': par_annual_incomes[i],
                                       'par_net_worth': par_net_worths[i], 'par_cibil_score': par_cibil_scores[i]}

            # print(par_names[1])
            i += 1

        # guarantors name
        guar_names = request.POST.getlist('guar_name[]')
        guar_annual_incomes = request.POST.getlist('guar_annual_income[]')
        guar_net_worths = request.POST.getlist('guar_net_worth[]')
        guar_cibil_scores = request.POST.getlist('guar_cibil_score[]')

        j = 0
        for x in range(len(guar_names)):
            guarantors_details[j] = {'guar_name': guar_names[j], 'guar_annual_income': guar_annual_incomes[j],
                                     'guar_net_worth': guar_net_worths[j], 'guar_cibil_score': guar_cibil_scores[j]}

            # print(guar_names[1])
            j += 1

        # nature of loan name
        nature_names = request.POST.getlist('nature_name[]')
        nature_by_partys = request.POST.getlist('nature_by_party[]')
        nature_by_branchs = request.POST.getlist('nature_by_branch[]')
        nature_by_hos = request.POST.getlist('nature_by_ho[]')

        l = 0
        for x in range(len(nature_names)):
            nature_of_loan_details[l] = {'nature_names': nature_names[l], 'nature_by_partys': nature_by_partys[l],
                                         'nature_by_branchs': nature_by_branchs[l], 'nature_by_hos': nature_by_hos[l]}

            l += 1

        saverecord1 = cust_comp_profile()


        #customer profile table
        saverecord1.cust_id = cust_id
        saverecord1.cust_name = cust_details.cust_name

        saverecord1.line_activity = request.POST.get('line_activity')
        saverecord1.constitution = request.POST.get('constitution')
        saverecord1.established_in = request.POST.get('established_in')
        saverecord1.dealing_with_us_since = request.POST.get('dealing_with_us_since')
        saverecord1.location = request.POST.get('location')
        saverecord1.priority = request.POST.get('priority')
        saverecord1.save()

        # Participants table



        t = 0
        for t in range(len(participants_details)):
            saverecord2 = participants()
            saverecord2.cust_id = cust_id
            saverecord2.cust_name = cust_details.cust_name
            saverecord2.par_name = participants_details[t]['par_name']
            saverecord2.par_annual_income = participants_details[t]['par_annual_income']
            saverecord2.par_net_worth = participants_details[t]['par_net_worth']
            saverecord2.par_cibil_score = participants_details[t]['par_cibil_score']
            saverecord2.save()
            t += 1

        # guarantors table

        f = 0
        for f in range(len(guarantors_details)):
            saverecord3 = guarantors()
            saverecord3.cust_id = cust_id
            saverecord3.cust_name = cust_details.cust_name
            saverecord3.guarantor_name = guarantors_details[f]['guar_name']
            saverecord3.guarantor_annual_income = guarantors_details[f]['guar_annual_income']
            saverecord3.guarantor_net_worth = guarantors_details[f]['guar_net_worth']
            saverecord3.guarantor_cibil_score = guarantors_details[f]['guar_cibil_score']
            saverecord3.save()
            f += 1


        #nature of loan table

        b = 0
        for b in range(len(nature_of_loan_details)):
            saverecord4 = loan_nature()
            saverecord4.cust_id = cust_id
            saverecord4.cust_name = cust_details.cust_name
            saverecord4.nature_name = nature_of_loan_details[b]['nature_names']
            saverecord4.nature_by_party = nature_of_loan_details[b]['nature_by_partys']
            saverecord4.nature_by_branch = nature_of_loan_details[b]['nature_by_branchs']
            saverecord4.nature_by_ho = nature_of_loan_details[b]['nature_by_hos']
            saverecord4.save()
            b += 1

        messages.success(request, 'Record Added Successfully')
        return HttpResponseRedirect(reverse('brief_profile_details'))
    else:
        messages.error(request, "Record insert unsuccessful")

    return render(request, 'LoanCore/brief_profile_form.html',
                  {'l_app_details': l_app_details, 'cust_details': cust_details})


def working_capital_limit_details(request):
    app_details = L_App.objects.select_related('cust_id')
    return render(request, 'LoanCore/working_capital_details.html', {'app_details': app_details})


def create_working_capital_limit(request, id):
    l_app_details = L_App.objects.get(l_id=id)
    l_app_id = l_app_details.app_id
    cust_id = str(l_app_details.cust_id)
    if request.method == 'POST':
        saverecord = working_capital()
        saverecord.cust_id = cust_id
        saverecord.app_id = l_app_id
        saverecord.nature_of_limit = request.POST.get('nature_of_limit')
        saverecord.purpose = request.POST.get('purpose')
        saverecord.amount = request.POST.get('amount')
        saverecord.margin = request.POST.get('margin')
        saverecord.prime_security = request.POST.get('prime_security')
        saverecord.collateral = request.POST.get('collateral')
        saverecord.roi_applicable = request.POST.get('roi_applicable')
        saverecord.roi_existing = request.POST.get('roi_existing')
        saverecord.roi_demand = request.POST.get('roi_demand')
        saverecord.roi_recommended = request.POST.get('roi_recommended')
        saverecord.save()
        messages.success(request, 'Record Added Successfully')
        return HttpResponseRedirect(reverse('working_capital_limit_details'))
    else:
        messages.error(request, "Record insert unsuccessful")
    return render(request, 'LoanCore/create_working_capital.html')


def term_details(request):
    app_details = L_App.objects.select_related('cust_id')
    return render(request, 'LoanCore/term_details.html', {'app_details': app_details})


def create_term(request, id):
    l_app_details = L_App.objects.get(l_id=id)
    l_app_id = l_app_details.app_id
    cust_id = str(l_app_details.cust_id)
    working_capital_details = working_capital.objects.get(app_id=l_app_id)
    purpose = working_capital_details.purpose
    amount = working_capital_details.amount
    if request.method == 'POST':
        saverecord = term_loan()
        saverecord.cust_id = cust_id
        saverecord.app_id = l_app_id
        saverecord.nature_of_limit = request.POST.get('nature_of_limit')
        saverecord.purpose = request.POST.get('purpose')
        saverecord.amount = request.POST.get('amount')
        saverecord.exposure = request.POST.get('exposure')
        saverecord.rate_of_interest = request.POST.get('rate_of_interest')
        saverecord.repayment = request.POST.get('repayment')
        saverecord.security_ties = request.POST.get('security_ties')
        saverecord.save()
        messages.success(request, 'Record Added Successfully')
        return HttpResponseRedirect(reverse('term_details'))
    else:
        messages.error(request, "Record insert unsuccessful")
    return render(request, 'LoanCore/create_term.html', {'app_details': l_app_details, 'purpose': purpose, 'amount': amount})


def nature_of_security_details(request):
    app_details = L_App.objects.select_related('cust_id')
    return render(request, 'LoanCore/nature_security_details.html', {'app_details': app_details})


def create_nature_of_security(request, id):
    l_app_details = L_App.objects.get(l_id=id)
    l_app_id = l_app_details.app_id
    cust_id = str(l_app_details.cust_id)
    if request.method == 'POST':
        saverecord = nature_security()
        saverecord.cust_id = cust_id
        saverecord.app_id = l_app_id
        saverecord.prime = request.POST.get('prime')
        saverecord.prime_description = request.POST.get('prime_description')
        saverecord.prime_existing = request.POST.get('prime_existing')
        saverecord.prime_proposed = request.POST.get('prime_proposed')
        saverecord.prime_recommended = request.POST.get('prime_recommended')
        saverecord.additional_collateral = request.POST.get('additional_collateral')
        saverecord.additional_collateral_description = request.POST.get('additional_description')
        saverecord.additional_collateral_existing = request.POST.get('additional_existing')
        saverecord.additional_collateral_proposed = request.POST.get('additional_proposed')
        saverecord.additional_collateral_recommended = request.POST.get('additional_recommended')
        saverecord.fresh = request.POST.get('fresh_collateral')
        saverecord.fresh_description = request.POST.get('fresh_description')
        saverecord.fresh_existing = request.POST.get('fresh_existing')
        saverecord.fresh_proposed = request.POST.get('fresh_proposed')
        saverecord.fresh_recommended = request.POST.get('fresh_recommended')
        saverecord.total_security_existing = request.POST.get('total_security_existing')
        saverecord.total_security_proposed = request.POST.get('total_security_proposed')
        saverecord.total_security_recommended = request.POST.get('total_security_recommended')
        saverecord.exposure_existing = request.POST.get('exposure_existing')
        saverecord.exposure_proposed = request.POST.get('exposure_proposed')
        saverecord.exposure_recommended = request.POST.get('exposure_recommended')
        saverecord.security_existing = request.POST.get('security_existing')
        saverecord.security_proposed = request.POST.get('security_proposed')
        saverecord.security_recommended = request.POST.get('security_recommended')
        saverecord.save()
        messages.success(request, 'Record Added Successfully')
        return HttpResponseRedirect(reverse('nature_of_security_details'))
    else:
        messages.error(request, "Record insert unsuccessful")

    return render(request, 'LoanCore/create_nature_security.html')


def gist_projection_details(request):
    app_details = L_App.objects.select_related('cust_id')
    return render(request, 'LoanCore/gist_projection_details.html', {'app_details': app_details})


def create_gist_projection(request, id):
    l_app_details = L_App.objects.get(l_id=id)
    l_app_id = l_app_details.app_id
    cust_id = str(l_app_details.cust_id)
    if request.method == 'POST':
        saverecord = gist_projection()
        saverecord.cust_id = cust_id
        saverecord.app_id = l_app_id
        saverecord.own_funds_first = request.POST.get('own_funds_first')
        saverecord.own_funds_second = request.POST.get('own_funds_second')
        saverecord.own_funds_third = request.POST.get('own_funds_third')
        saverecord.own_funds_fourth = request.POST.get('own_funds_fourth')
        saverecord.cash_credit_first = request.POST.get('cash_credit_first')
        saverecord.cash_credit_second = request.POST.get('cash_credit_second')
        saverecord.cash_credit_third = request.POST.get('cash_credit_third')
        saverecord.cash_credit_fourth = request.POST.get('cash_credit_fourth')
        saverecord.term_loan_first = request.POST.get('term_loan_first')
        saverecord.term_loan_second = request.POST.get('term_loan_second')
        saverecord.term_loan_third = request.POST.get('term_loan_third')
        saverecord.term_loan_fourth = request.POST.get('term_loan_fourth')
        saverecord.sales_first = request.POST.get('sales_first')
        saverecord.sales_second = request.POST.get('sales_second')
        saverecord.sales_third = request.POST.get('sales_third')
        saverecord.sales_fourth = request.POST.get('sales_fourth')
        saverecord.gross_profit_first = request.POST.get('gross_profit_first')
        saverecord.gross_profit_second = request.POST.get('gross_profit_second')
        saverecord.gross_profit_third = request.POST.get('gross_profit_third')
        saverecord.gross_profit_fourth = request.POST.get('gross_profit_fourth')
        saverecord.interest_term_loan_first = request.POST.get('interest_term_loan_first')
        saverecord.interest_term_loan_second = request.POST.get('interest_term_loan_second')
        saverecord.interest_term_loan_third = request.POST.get('interest_term_loan_third')
        saverecord.interest_term_loan_fourth = request.POST.get('interest_term_loan_fourth')
        saverecord.depriciation_first = request.POST.get('depriciation_first')
        saverecord.depriciation_second = request.POST.get('depriciation_second')
        saverecord.depriciation_third = request.POST.get('depriciation_third')
        saverecord.depriciation_fourth = request.POST.get('depriciation_fourth')
        saverecord.cash_accrual_first = request.POST.get('cash_accrual_first')
        saverecord.cash_accrual_second = request.POST.get('cash_accrual_second')
        saverecord.cash_accrual_third = request.POST.get('cash_accrual_third')
        saverecord.cash_accrual_fourth = request.POST.get('cash_accrual_fourth')
        saverecord.repayment_first = request.POST.get('repayment_first')
        saverecord.repayment_second = request.POST.get('repayment_second')
        saverecord.repayment_third = request.POST.get('repayment_third')
        saverecord.repayment_fourth = request.POST.get('repayment_fourth')
        saverecord.interest_first = request.POST.get('interest_first')
        saverecord.interest_second = request.POST.get('interest_second')
        saverecord.interest_third = request.POST.get('interest_third')
        saverecord.interest_fourth = request.POST.get('interest_fourth')
        saverecord.dscr_first = request.POST.get('dscr_first')
        saverecord.dscr_second = request.POST.get('dscr_second')
        saverecord.dscr_third = request.POST.get('dscr_third')
        saverecord.dscr_fourth = request.POST.get('dscr_fourth')
        saverecord.save()
        messages.success(request, 'Record Added Successfully')
        return HttpResponseRedirect(reverse('gist_projection_details'))
    else:
        messages.error(request, "Record insert unsuccessful")

    return render(request, 'LoanCore/create_gist_projection.html')


def financial_analysis_details(request):
    app_details = L_App.objects.select_related('cust_id')
    return render(request, 'LoanCore/financial_analysis_details.html', {'app_details': app_details})


def create_financial_analysis(request, id):
    l_app_details = L_App.objects.get(l_id=id)
    l_app_id = l_app_details.app_id
    cust_id = str(l_app_details.cust_id)
    if request.method == 'POST':
        saverecord = financial_analysis()
        saverecord.cust_id = cust_id
        saverecord.app_id = l_app_id
        saverecord.capital_first = request.POST.get('capital_first')
        saverecord.capital_second = request.POST.get('capital_second')
        saverecord.capital_third = request.POST.get('capital_third')
        saverecord.capital_projected = request.POST.get('capital_projected')
        saverecord.reserves_first = request.POST.get('reserves_first')
        saverecord.reserves_second = request.POST.get('reserves_second')
        saverecord.reserves_third = request.POST.get('reserves_third')
        saverecord.reserves_projected = request.POST.get('reserves_projected')
        saverecord.unsecured_loan_1_first = request.POST.get('unsecured_loan_1_first')
        saverecord.unsecured_loan_1_second = request.POST.get('unsecured_loan_1_second')
        saverecord.unsecured_loan_1_third = request.POST.get('unsecured_loan_1_third')
        saverecord.unsecured_loan_1_projected = request.POST.get('unsecured_loan_1_projected')
        saverecord.unsecured_loan_others_first = request.POST.get('unsecured_loan_others_first')
        saverecord.unsecured_loan_others_second = request.POST.get('unsecured_loan_others_second')
        saverecord.unsecured_loan_others_third = request.POST.get('unsecured_loan_others_third')
        saverecord.unsecured_loan_others_projected = request.POST.get('unsecured_loan_others_projected')
        saverecord.gross_profit_first = request.POST.get('gross_profit_first')
        saverecord.gross_profit_second = request.POST.get('gross_profit_second')
        saverecord.gross_profit_third = request.POST.get('gross_profit_third')
        saverecord.gross_profit_first_projected = request.POST.get('gross_profit_first_projected')
        saverecord.interest_term_on_loan_first = request.POST.get('interest_term_on_loan_first')
        saverecord.interest_term_on_loan_second = request.POST.get('interest_term_on_loan_second')
        saverecord.interest_term_on_loan_third = request.POST.get('interest_term_on_loan_third')
        saverecord.interest_term_on_loan_projected = request.POST.get('interest_term_on_loan_projected')
        saverecord.total_capital_fund_first = request.POST.get('total_capital_fund_first')
        saverecord.total_capital_fund_second = request.POST.get('total_capital_fund_second')
        saverecord.total_capital_fund_third = request.POST.get('total_capital_fund_third')
        saverecord.total_capital_fund_projected = request.POST.get('total_capital_fund_projected')
        saverecord.term_loan_bank_first = request.POST.get('term_loan_bank_first')
        saverecord.term_loan_bank_second = request.POST.get('term_loan_bank_second')
        saverecord.term_loan_bank_third = request.POST.get('term_loan_bank_third')
        saverecord.term_loan_bank_projected = request.POST.get('term_loan_bank_projected')
        saverecord.term_loan_others_first = request.POST.get('term_loan_others_first')
        saverecord.term_loan_others_second = request.POST.get('term_loan_others_second')
        saverecord.term_loan_others_third = request.POST.get('term_loan_others_third')
        saverecord.term_loan_others_projected = request.POST.get('term_loan_others_projected')
        saverecord.wcl_first = request.POST.get('wcl_first')
        saverecord.wcl_second = request.POST.get('wcl_second')
        saverecord.wcl_third = request.POST.get('wcl_third')
        saverecord.wcl_projected = request.POST.get('wcl_projected')
        saverecord.sundry_first = request.POST.get('sundry_first')
        saverecord.sundry_second = request.POST.get('sundry_second')
        saverecord.sundry_third = request.POST.get('sundry_third')
        saverecord.sundry_projected = request.POST.get('sundry_projected')
        saverecord.other_current_liabilities_first = request.POST.get('other_current_liabilities_first')
        saverecord.other_current_liabilities_second = request.POST.get('other_current_liabilities_second')
        saverecord.other_current_liabilities_third = request.POST.get('other_current_liabilities_third')
        saverecord.other_current_liabilities_projected = request.POST.get('other_current_liabilities_projected')
        saverecord.total_current_liabilities_first = request.POST.get('total_current_liabilities_first')
        saverecord.total_current_liabilities_second = request.POST.get('total_current_liabilities_second')
        saverecord.total_current_liabilities_third = request.POST.get('total_current_liabilities_third')
        saverecord.total_current_liabilities_projected = request.POST.get('total_current_liabilities_projected')
        saverecord.fixed_asset_first = request.POST.get('fixed_asset_first')
        saverecord.fixed_asset_second = request.POST.get('fixed_asset_second')
        saverecord.fixed_asset_third = request.POST.get('fixed_asset_third')
        saverecord.fixed_asset_projected = request.POST.get('fixed_asset_projected')
        saverecord.investments_first = request.POST.get('investments_first')
        saverecord.investments_second = request.POST.get('investments_second')
        saverecord.investments_third = request.POST.get('investments_third')
        saverecord.investments_projected = request.POST.get('investments_projected')
        saverecord.inter_firm_investments_first = request.POST.get('inter_firm_investments_first')
        saverecord.inter_firm_investments_second = request.POST.get('inter_firm_investments_second')
        saverecord.inter_firm_investments_third = request.POST.get('inter_firm_investments_third')
        saverecord.inter_firm_investments_projected = request.POST.get('inter_firm_investments_projected')
        saverecord.loan_advance_first = request.POST.get('loan_advance_first')
        saverecord.loan_advance_second = request.POST.get('loan_advance_second')
        saverecord.loan_advance_third = request.POST.get('loan_advance_third')
        saverecord.loan_advance_projected = request.POST.get('loan_advance_projected')
        saverecord.non_current_assets_first = request.POST.get('non_current_assets_first')
        saverecord.non_current_assets_second = request.POST.get('non_current_assets_second')
        saverecord.non_current_assets_third = request.POST.get('non_current_assets_third')
        saverecord.non_current_assets_projected = request.POST.get('non_current_assets_projected')
        saverecord.s_debtors_first = request.POST.get('s_debtors_first')
        saverecord.s_debtors_second = request.POST.get('s_debtors_second')
        saverecord.s_debtors_third = request.POST.get('s_debtors_third')
        saverecord.s_debtors_projected = request.POST.get('s_debtors_projected')
        saverecord.total_stock_first = request.POST.get('total_stock_first')
        saverecord.total_stock_second = request.POST.get('total_stock_second')
        saverecord.total_stock_third = request.POST.get('total_stock_third')
        saverecord.total_stock_projected = request.POST.get('total_stock_projected')
        saverecord.other_current_assets_first = request.POST.get('other_current_assets_first')
        saverecord.other_current_assets_second = request.POST.get('other_current_assets_second')
        saverecord.other_current_assets_third = request.POST.get('other_current_assets_third')
        saverecord.other_current_assets_projected = request.POST.get('other_current_assets_projected')
        saverecord.total_current_assets_first = request.POST.get('total_current_assets_first')
        saverecord.total_current_assets_second = request.POST.get('total_current_assets_second')
        saverecord.total_current_assets_third = request.POST.get('total_current_assets_third')
        saverecord.total_current_assets_projected = request.POST.get('total_current_assets_projected')
        saverecord.total_current_liab_first = request.POST.get('total_current_liab_first')
        saverecord.total_current_liab_second = request.POST.get('total_current_liab_second')
        saverecord.total_current_liab_third = request.POST.get('total_current_liab_third')
        saverecord.total_current_liab_projected = request.POST.get('total_current_liab_projected')
        saverecord.net_sales_first = request.POST.get('net_sales_first')
        saverecord.net_sales_second = request.POST.get('net_sales_second')
        saverecord.net_sales_third = request.POST.get('net_sales_third')
        saverecord.net_sales_projected = request.POST.get('net_sales_projected')
        saverecord.net_purchases_first = request.POST.get('net_purchases_first')
        saverecord.net_purchases_second = request.POST.get('net_purchases_second')
        saverecord.net_purchases_third = request.POST.get('net_purchases_third')
        saverecord.net_purchases_projected = request.POST.get('net_purchases_projected')
        saverecord.gross_profit1_first = request.POST.get('gross_profit1_first')
        saverecord.gross_profit1_second = request.POST.get('gross_profit1_second')
        saverecord.gross_profit1_third = request.POST.get('gross_profit1_third')
        saverecord.gross_profit1_projected = request.POST.get('gross_profit1_projected')
        saverecord.interest_other_than_bank_loans_first = request.POST.get('interest_other_than_bank_loans_first')
        saverecord.interest_other_than_bank_loans_second = request.POST.get('interest_other_than_bank_loans_second')
        saverecord.interest_other_than_bank_loans_third = request.POST.get('interest_other_than_bank_loans_third')
        saverecord.interest_other_than_bank_loans_projected = request.POST.get('interest_other_than_bank_loans_projected')
        saverecord.interest_on_bank_loans_first = request.POST.get('interest_on_bank_loans_first')
        saverecord.interest_on_bank_loans_second = request.POST.get('interest_on_bank_loans_second')
        saverecord.interest_on_bank_loans_third = request.POST.get('interest_on_bank_loans_third')
        saverecord.interest_on_bank_loans_projected = request.POST.get('interest_on_bank_loans_projected')
        saverecord.interest_partner_director_loan_first = request.POST.get('interest_partner_director_loan_first')
        saverecord.interest_partner_director_loan_second = request.POST.get('interest_partner_director_loan_second')
        saverecord.interest_partner_director_loan_third = request.POST.get('interest_partner_director_loan_third')
        saverecord.interest_partner_director_loan_projected = request.POST.get('interest_partner_director_loan_projected')
        saverecord.depreciation_first = request.POST.get('depreciation_first')
        saverecord.depreciation_second = request.POST.get('depreciation_second')
        saverecord.depreciation_third = request.POST.get('depreciation_third')
        saverecord.depreciation_projected = request.POST.get('depreciation_projected')
        saverecord.partner_salary_first = request.POST.get('partner_salary_first')
        saverecord.partner_salary_second = request.POST.get('partner_salary_second')
        saverecord.partner_salary_third = request.POST.get('partner_salary_third')
        saverecord.partner_salary_projected = request.POST.get('partner_salary_projected')
        saverecord.net_profit_first = request.POST.get('net_profit_first')
        saverecord.net_profit_second = request.POST.get('net_profit_second')
        saverecord.net_profit_third = request.POST.get('net_profit_third')
        saverecord.net_profit_projected = request.POST.get('net_profit_projected')
        saverecord.total_cash_generation_first = request.POST.get('total_cash_generation_first')
        saverecord.total_cash_generation_second = request.POST.get('total_cash_generation_second')
        saverecord.total_cash_generation_third = request.POST.get('total_cash_generation_third')
        saverecord.total_cash_generation_projected = request.POST.get('total_cash_generation_projected')
        saverecord.turnover_ratio_first = request.POST.get('turnover_ratio_first')
        saverecord.turnover_ratio_second = request.POST.get('turnover_ratio_second')
        saverecord.turnover_ratio_third = request.POST.get('turnover_ratio_third')
        saverecord.turnover_ratio_projected = request.POST.get('turnover_ratio_projected')
        saverecord.debtors_velocity_first = request.POST.get('debtors_velocity_first')
        saverecord.debtors_velocity_second = request.POST.get('debtors_velocity_second')
        saverecord.debtors_velocity_third = request.POST.get('debtors_velocity_third')
        saverecord.debtors_velocity_projected = request.POST.get('debtors_velocity_projected')
        saverecord.creditors_velocity_first = request.POST.get('creditors_velocity_first')
        saverecord.creditors_velocity_second = request.POST.get('creditors_velocity_second')
        saverecord.creditors_velocity_third = request.POST.get('creditors_velocity_third')
        saverecord.creditors_velocity_projected = request.POST.get('creditors_velocity_projected')
        saverecord.current_ratio_first = request.POST.get('current_ratio_first')
        saverecord.current_ratio_second = request.POST.get('current_ratio_second')
        saverecord.current_ratio_third = request.POST.get('current_ratio_third')
        saverecord.current_ratio_projected = request.POST.get('current_ratio_projected')
        saverecord.debt_equity_ratio_first = request.POST.get('debt_equity_ratio_first')
        saverecord.debt_equity_ratio_second = request.POST.get('debt_equity_ratio_second')
        saverecord.debt_equity_ratio_third = request.POST.get('debt_equity_ratio_third')
        saverecord.debt_equity_ratio_projected = request.POST.get('debt_equity_ratio_projected')
        saverecord.mpbf_method_1_first = request.POST.get('mpbf_method_1_first')
        saverecord.mpbf_method_1_second = request.POST.get('mpbf_method_1_second')
        saverecord.mpbf_method_1_third = request.POST.get('mpbf_method_1_third')
        saverecord.mpbf_method_1_projected = request.POST.get('mpbf_method_1_projected')
        saverecord.mpbf_turnover_basis_first = request.POST.get('mpbf_turnover_basis_first')
        saverecord.mpbf_turnover_basis_second = request.POST.get('mpbf_turnover_basis_second')
        saverecord.mpbf_turnover_basis_third = request.POST.get('mpbf_turnover_basis_third')
        saverecord.mpbf_turnover_basis_projected = request.POST.get('mpbf_turnover_basis_projected')
        saverecord.drawing_power_first = request.POST.get('drawing_power_first')
        saverecord.drawing_power_second = request.POST.get('drawing_power_second')
        saverecord.drawing_power_third = request.POST.get('drawing_power_third')
        saverecord.drawing_power_projected = request.POST.get('drawing_power_projected')
        saverecord.dscr_first = request.POST.get('dscr_first')
        saverecord.dscr_second = request.POST.get('dscr_second')
        saverecord.dscr_third = request.POST.get('dscr_third')
        saverecord.dscr_projected = request.POST.get('dscr_fourth')
        saverecord.save()
        messages.success(request, 'Record Added Successfully')
        return HttpResponseRedirect(reverse('financial_analysis_details'))
    else:
        messages.error(request, "Record insert unsuccessful")

    return render(request, 'LoanCore/create_financial_analysis.html')



