from django import forms
from .models import *


class CustomerDetailsForm(forms.ModelForm):
    class Meta:
        model = cust_detail
        fields = ['cust_id', 'cust_name', 'cust_email', 'cust_dob', 'cust_address', 'cust_tel1', 'cust_tel2']

        labels = {
            "cust_id": "Customer ID",
            "cust_name": "Customer Name",

            "cust_email": "Email",
            "cust_dob": "Date Of Birth",
            "cust_address": "Address",
            "cust_tel1": "Mobile 1",
            "cust_tel2": "Mobile 2"
        }
